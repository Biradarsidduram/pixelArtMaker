
const submit = document.querySelector('input:not([id])');
const table = document.getElementById('pixelCanvas');
function makeGrid() {
	const width = document.getElementById('inputWeight').value;
	const height = document.getElementById('inputHeight').value;	
	// Your code goes here!
	//no.of tr = height of the table
	if(width<100 || height<100){
      if(table.childElementCount!=0){
		var listItems = table.children;
 		for(let p =listItems.length-1 ; p>=0;p--){
 			table.removeChild(listItems[p]);
		}
	}
	for(let i  = 0;i<height;i++)
	{
		var tr = document.createElement('tr');
		table.appendChild(tr);
		for(let j=0;j<width;j++){
 			var td = document.createElement('td');
 			tr.appendChild(td);
 		}
	}
  }
  else{
    alert('Please Do not add more than 100 width and height ');
  }
}

// When size is submitted by the user, call makeGrid()
submit.addEventListener('click' , function (event){
	event.preventDefault();
	makeGrid();
 });
table.addEventListener('click', function (){
	if(event.target.tagName==='TD'){
 		 event.target.style.backgroundColor = document.getElementById('colorPicker').value;
	}
});
table.addEventListener('dblclick',function(){
     if(event.target.tagName==='TD'){
 		 event.target.style.backgroundColor = 'inherit';
     }
});
table.ondragover = function(event) {
     if(event.target.tagName==='TD'){
 		 event.target.style.backgroundColor = document.getElementById('colorPicker').value;
 	}
};